#include "day2impl.h"

#include <format>
#include <fstream>
#include <iostream>

int main([[maybe_unused]] int argc, [[maybe_unused]] char* argv[])
{
    {
        std::ifstream inputStream("input.txt");
        SubmarinePosition finalPos = ProcessMovements<SubmarinePosition>(ParseMovementStream(std::move(inputStream)));
        std::cout << std::format("Part 1:\nFinal position:\nHoriz: {}\nDepth: {}\nProduct: {}\n\n", finalPos.GetHoriz(), finalPos.GetDepth(), finalPos.GetProduct());
    }

    {
        std::ifstream inputStream("input.txt");
        SubmarineState finalState = ProcessMovements<SubmarineState>(ParseMovementStream(std::move(inputStream)));
        std::cout << std::format("Part 2:\nFinal state:\nHoriz: {}\nDepth: {}\nProduct: {}\n", finalState.GetHoriz(), finalState.GetDepth(), finalState.GetProduct());
    }

    return 0;
}
