#include "day2impl.h"

#include <algorithm>
#include <istream>
#include <sstream>

std::vector<MovementVariant> ParseMovementStream(std::istream&& inputStream)
{
    std::vector<MovementVariant> res;

    while (inputStream.good())
    {
        std::string verb;
        int val = {};
        inputStream >> verb >> val;
        if (verb.compare("forward") == 0)
        {
            res.push_back(MoveForward{val});
        }
        else if (verb.compare("up") == 0)
        {
            res.push_back(MoveUp{val});
        }
        else if (verb.compare("down") == 0)
        {
            res.push_back(MoveDown{val});
        }
    }

    return res;
}
