#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#include "day2impl.h"

std::string k_testData = R"(
forward 5
down 5
forward 8
up 3
down 8
forward 2
)";

TEST_CASE("Day2 Test")
{
    SubmarinePosition testPos;
    testPos += MoveForward{5};
    REQUIRE(testPos.GetHoriz() == 5);
    REQUIRE(testPos.GetDepth() == 0);
    testPos += MoveDown{5};
    REQUIRE(testPos.GetHoriz() == 5);
    REQUIRE(testPos.GetDepth() == 5);
    testPos += MoveForward{8};
    REQUIRE(testPos.GetHoriz() == 13);
    REQUIRE(testPos.GetDepth() == 5);
    testPos += MoveUp{3};
    REQUIRE(testPos.GetHoriz() == 13);
    REQUIRE(testPos.GetDepth() == 2);
    testPos += MoveDown{8};
    REQUIRE(testPos.GetHoriz() == 13);
    REQUIRE(testPos.GetDepth() == 10);
    testPos += MoveForward{2};
    REQUIRE(testPos.GetHoriz() == 15);
    REQUIRE(testPos.GetDepth() == 10);

    SubmarinePosition subPos = ProcessMovements<SubmarinePosition>(ParseMovementStream(std::stringstream(k_testData)));
    REQUIRE(subPos.GetHoriz() == 15);
    REQUIRE(subPos.GetDepth() == 10);
    REQUIRE(subPos.GetProduct() == 150);

    SubmarineState subState = ProcessMovements<SubmarineState>(ParseMovementStream(std::stringstream(k_testData)));
    REQUIRE(subState.GetHoriz() == 15);
    REQUIRE(subState.GetDepth() == 60);
    REQUIRE(subState.GetProduct() == 900);
}
