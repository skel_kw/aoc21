#pragma once

#include <istream>
#include <numeric>
#include <variant>
#include <vector>

template <class Tag> struct StateChange
{
	int mag = {};
};
struct MoveDown : public StateChange<MoveDown> {};
struct MoveUp : public StateChange<MoveUp> {};
struct MoveForward : public StateChange<MoveForward> {};
using MovementVariant = std::variant<MoveDown, MoveUp, MoveForward>;

class SubmarinePosition
{
	std::int64_t horiz = {};
	std::int64_t depth = {};

public:

	std::int64_t GetHoriz() const { return horiz; }
	std::int64_t GetDepth() const { return depth; }
	std::int64_t GetProduct() const { return horiz * depth; }

	void ApplyMovement(MoveDown m)
	{
		depth += m.mag;
	}

	void ApplyMovement(MoveUp m)
	{
		depth -= m.mag;
	}

	void ApplyMovement(MoveForward m)
	{
		horiz += m.mag;
	}

	friend SubmarinePosition& operator+=(SubmarinePosition& p, MovementVariant const& mv)
	{
		std::visit([&p](auto const& m) { p.ApplyMovement(m); }, mv);
		return p;
	}

	friend SubmarinePosition operator+(SubmarinePosition const& p, MovementVariant const& mv)
	{
		SubmarinePosition res = p;
		res += mv;
		return res;
	}
};

class SubmarineState
{
	std::int64_t horiz = {};
	std::int64_t depth = {};
	int aim = {};

public:

	std::int64_t GetHoriz() const { return horiz; }
	std::int64_t GetDepth() const { return depth; }
	std::int64_t GetProduct() const { return horiz * depth; }

	void ApplyMovement(MoveDown m)
	{
		aim += m.mag;
	}

	void ApplyMovement(MoveUp m)
	{
		aim -= m.mag;
	}

	void ApplyMovement(MoveForward m)
	{
		horiz += m.mag;
		depth += m.mag * aim;
	}

	friend SubmarineState& operator+=(SubmarineState& s, MovementVariant const& mv)
	{
		std::visit([&s](auto const& m) { s.ApplyMovement(m); }, mv);
		return s;
	}

	friend SubmarineState operator+(SubmarineState const& s, MovementVariant const& mv)
	{
		SubmarineState res = s;
		res += mv;
		return res;
	}
};

std::vector<MovementVariant> ParseMovementStream(std::istream&& inputStream);

template <class StateType>
StateType ProcessMovements(std::vector<MovementVariant> const& movements)
{
    return std::accumulate(std::begin(movements), std::end(movements), StateType{});
}
