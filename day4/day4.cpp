#include "day4impl.h"

#include <format>
#include <fstream>
#include <iostream>

int main([[maybe_unused]] int argc, [[maybe_unused]] char* argv[])
{
    {
        std::ifstream inputStream("input.txt");
        std::vector<int> bingoNumbers = ReadNumbers(inputStream);
        std::vector<BingoBoard> bingoBoards = ReadBoards(inputStream);

        std::reverse(std::begin(bingoNumbers), std::end(bingoNumbers)); //reverse the numbers, so they can be popped from the back

        //feed the board numbers until one wins
        bool bingo = false;
        for (; !bingoNumbers.empty(); bingoNumbers.pop_back())
        {
            int const nextNumber = bingoNumbers.back();
            for (auto& board : bingoBoards)
            {
                board.MatchValue(nextNumber);
                if (board.IsMatched())
                {
                    auto const winningBoard = std::distance(&bingoBoards[0], &board);
                    std::cout << std::format("Part 4a:\nThe first board to win is: {}\nThe final score for this board is: {}\n\n", winningBoard, board.Result());
                    bingo = true;
                    break;
                }
            }
            if (bingo)
            {
                break;
            }
        }
    }

    //for the second part, work out which board will be the last to win, and what is its final score
    {
        std::ifstream inputStream("input.txt");
        std::vector<int> bingoNumbers = ReadNumbers(inputStream);
        std::vector<BingoBoard> bingoBoards = ReadBoards(inputStream);

        std::reverse(std::begin(bingoNumbers), std::end(bingoNumbers)); //reverse the numbers, so they can be popped from the back

        //feed the board numbers until one wins
        for (; !bingoNumbers.empty(); bingoNumbers.pop_back())
        {
            int const nextNumber = bingoNumbers.back();
            for (auto& board : bingoBoards)
            {
                board.MatchValue(nextNumber);
            }

            if (bingoBoards.size() == 1 && bingoBoards[0].IsMatched())
            {
                std::cout << std::format("Part 4b:\nThe last board to win has a final score of: {}\n\n", bingoBoards[0].Result());
                break;
            }

            std::erase_if(bingoBoards, [](BingoBoard const& b) { return b.IsMatched(); });
        }
    }

    return 0;
}
