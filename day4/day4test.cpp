#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#include "day4impl.h"

const char* k_testData = R"(
7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

22 13 17 11  0
 8  2 23  4 24
21  9 14 16  7
 6 10  3 18  5
 1 12 20 15 19

 3 15  0  2 22
 9 18 13 17  5
19  8  7 25 23
20 11 10 24  4
14 21 16 12  6

14 21 17 24  4
10 16 15  9 19
18  8 23 26 20
22 11 13  6  5
 2  0 12  3  7
)";

TEST_CASE("Day4a Test")
{
	auto inputData = std::stringstream(k_testData);
	std::vector<int> bingoNumbers = ReadNumbers(inputData);
	std::vector<BingoBoard> bingoBoards = ReadBoards(inputData);

	REQUIRE(bingoNumbers.size() == 27);
	REQUIRE(bingoBoards.size() == 3);

	std::reverse(std::begin(bingoNumbers), std::end(bingoNumbers)); //reverse the numbers, so they can be popped from the back
	for (int i = 0; i < 5; i++)
	{
		int const num = bingoNumbers.back();
		bingoNumbers.pop_back();
		for (auto& board : bingoBoards)
		{
			board.MatchValue(num);
		}
	}
	for (auto const& board : bingoBoards)
	{
		REQUIRE(board.IsMatched() == false);
	}

	//TODO test the boards to see that the correct numbers are marked

	for (int i = 0; i < 6; i++)
	{
		int const num = bingoNumbers.back();
		bingoNumbers.pop_back();
		for (auto& board : bingoBoards)
		{
			board.MatchValue(num);
		}
	}
	for (auto const& board : bingoBoards)
	{
		REQUIRE(board.IsMatched() == false);
	}


	int const lastDrawnNum = bingoNumbers.back();
	bingoNumbers.pop_back();
	for (auto& board : bingoBoards)
	{
		board.MatchValue(lastDrawnNum);
	}

	REQUIRE(bingoBoards[0].IsMatched() == false);
	REQUIRE(bingoBoards[1].IsMatched() == false);
	REQUIRE(bingoBoards[2].IsMatched() == true);

	REQUIRE(bingoBoards[2].SumOfUnmarked() == 188);
	REQUIRE(bingoBoards[2].LastDrawnNumber() == 24);
	REQUIRE(bingoBoards[2].Result() == 4512);
}

TEST_CASE("Day4b Test")
{
	auto inputData = std::stringstream(k_testData);
	std::vector<int> bingoNumbers = ReadNumbers(inputData);
	std::vector<BingoBoard> bingoBoards = ReadBoards(inputData);

	REQUIRE(bingoNumbers.size() == 27);
	REQUIRE(bingoBoards.size() == 3);

	std::reverse(std::begin(bingoNumbers), std::end(bingoNumbers)); //reverse the numbers, so they can be popped from the back

	//feed the board numbers until one wins
	for (; !bingoNumbers.empty(); bingoNumbers.pop_back())
	{
		int const nextNumber = bingoNumbers.back();
		for (auto& board : bingoBoards)
		{
			board.MatchValue(nextNumber);
		}

		if (bingoBoards.size() == 1 && bingoBoards[0].IsMatched())
		{
			REQUIRE(nextNumber == 13);
			REQUIRE(bingoBoards[0].Result() == 1924);
			break;
		}

		std::erase_if(bingoBoards, [](BingoBoard const& b) { return b.IsMatched(); });
	}

}
