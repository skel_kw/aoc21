#include "day4impl.h"

#include <algorithm>
#include <format>
#include <numeric>
#include <sstream>

BoardMatrix BoardMatrix::transpose() const
{
    BoardMatrix res;
    for (int r = 0; r < BoardSize; r++)
    {
        for (int c = 0; c < BoardSize; c++)
        {
            res.rows[c].elements[r] = rows[r].elements[c];
        }
    }
    return res;
}

void BingoBoard::MatchValue(int val)
{
    for (auto& row : values.rows)
    {
        for (auto& elem : row.elements)
        {
            if (elem.first == val)
            {
                elem.second.val = true;
            }
        }
    }
    lastDrawnNumber = val;

    UpdateMatched();
}

int BingoBoard::SumOfUnmarked() const
{
    int res = {};
    for (auto const& row : values.rows)
    {
        res += std::accumulate(std::begin(row.elements), std::end(row.elements), 0,
            [](int v1, ElementPair const& elem) -> int {
                return v1 + ((elem.second == false) ? elem.first : 0);
            });
    }
    return res;
}

void BingoBoard::UpdateMatched()
{
    if (matched)
    {
        return;
    }

    for (auto const& row : values.rows)
    {
        if (std::all_of(std::begin(row.elements), std::end(row.elements), [](auto const& elem) { return elem.second; }))
        {
            matched = true;
            return;
        }
    }

    auto transposedValues = values.transpose();
    for (auto const& row : transposedValues.rows)
    {
        if (std::all_of(std::begin(row.elements), std::end(row.elements), [](auto const& elem) { return elem.second; }))
        {
            matched = true;
            return;
        }
    }
}

std::vector<int> ReadNumbers(std::istream& inputStream)
{
    //inputStream >> std::ws;
    if (inputStream.bad())
    {
        throw std::exception("Input buffer not valid");
    }

    //Read a line of comma separated numbers
    std::string readBuffer;
    std::getline(inputStream >> std::ws, readBuffer);
    if (inputStream.bad())
    {
        throw std::exception("Input buffer error when reading numbers");
    }

    //Replace commas with spaces to utilise istream's parsing of whitespace
    constexpr auto Separator = ',';
    std::replace(std::begin(readBuffer), std::end(readBuffer), Separator, ' ');

    std::stringstream strStream(readBuffer);

    std::vector<int> res;
    std::copy(std::istream_iterator<int>{ strStream }, std::istream_iterator<int>{}, std::back_inserter(res));
    return res;
}

std::vector<BingoBoard> ReadBoards(std::istream& inputStream)
{
    std::vector<BingoBoard> res;
    //Read remaining stream to extract boards
    while (inputStream.good())
    {
        BoardMatrix inputValues;
        for (int r = 0; r < BoardSize; r++)
        {
            for (int c = 0; c < BoardSize; c++)
            {
                int val = {};
                inputStream >> val;
                inputValues.rows[r].elements[c] = ElementPair(val, IsMatched{ false });
            }
        }
        res.emplace_back(std::move(inputValues));

        inputStream >> std::ws; //consume trailing whitespace incase there EOS is lurking after it
    }

    return res;
}
