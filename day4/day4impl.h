#pragma once

#include <array>
#include <istream>
#include <utility>
#include <vector>

constexpr int BoardSize = 5;

struct IsMatched
{
    bool val;

    operator bool() const { return val; }
};

using ElementPair = std::pair<int, IsMatched>;

struct BoardRow
{
    std::array<ElementPair, BoardSize> elements;

    BoardRow() = default;

    BoardRow(int a, int b, int c, int d, int e)
    {
        elements[0] = ElementPair(a, IsMatched{false});
        elements[1] = ElementPair(b, IsMatched{false});
        elements[2] = ElementPair(c, IsMatched{false});
        elements[3] = ElementPair(d, IsMatched{false});
        elements[4] = ElementPair(e, IsMatched{false});
    }
};

struct BoardMatrix
{
    std::array<BoardRow, BoardSize> rows;

    BoardMatrix transpose() const;
};

class BingoBoard
{
public:
    BingoBoard(BoardMatrix board) : values(std::move(board)) {}

    bool IsMatched() const { return matched; }
    void MatchValue(int val);

    int SumOfUnmarked() const;
    int LastDrawnNumber() const { return lastDrawnNumber; }
    int Result() const { return SumOfUnmarked() * LastDrawnNumber(); }

private:
    void UpdateMatched();

private:
    BoardMatrix values;
    int lastDrawnNumber = -1;
    bool matched = false; //cache whether the board is matched for quicker IsMatched() query of already matched boards
};

std::vector<int> ReadNumbers(std::istream& inputStream);
std::vector<BingoBoard> ReadBoards(std::istream& inputStream);
