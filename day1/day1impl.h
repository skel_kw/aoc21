#pragma once

#include <algorithm>
#include <format>
#include <iostream>
#include <numeric>
#include <vector>

std::vector<int> readFromHttpSocket();
std::vector<int> readFromTestData();
std::vector<int> readFromInputFile();

template <int SlidingWindowSize, bool Verbose>
int processData(std::vector<int> const& depths)
{
    int incrementCount = 0;

    constexpr int kUnset = std::numeric_limits<int>::min();
    int prev = kUnset;
    for (std::size_t idx = 0; idx < depths.size() - (SlidingWindowSize-1); ++idx)
    {
        auto const slidingSum = std::accumulate(&depths[idx], &depths[idx] + SlidingWindowSize, 0);
        if (prev == kUnset)
        {
            if constexpr (Verbose)
            {
                std::cout << slidingSum << " (N/A - no previous sum)\n";
            }
        }
        else
        {
            const int delta = slidingSum - prev;
            if (delta > 0)
            {
                incrementCount++;
            }

            if constexpr (Verbose)
            {
                std::cout << std::format("{} {}\n", slidingSum, (delta > 0) ? "(increased)" : (delta < 0) ? "(decreased)" : "(no change)");
            }
        }
        prev = slidingSum;
    }

    return incrementCount;
}
