#include "day1impl.h"

#include <algorithm>
#include <iostream>
#include <iterator>
#include <format>
#include <fstream>
#include <numeric>
#include <sstream>
#include <string>
#include <vector>

//#include <boost/asio/ip/tcp.hpp>

std::vector<int> readFromHttpSocket()
{
    //constexpr const char* kServer = "adventofcode.com";
    //constexpr const char* kRequest = "/2021/day/1/input";

    //boost::asio::ip::tcp::iostream inputStream;

    //// The entire sequence of I/O operations must complete within 60 seconds.
    //// If an expiry occurs, the socket is automatically closed and the stream
    //// becomes bad.
    //inputStream.expires_after(std::chrono::seconds(60));

    //// Establish a connection to the server.
    //inputStream.connect(kServer, "https");
    //if (!inputStream)
    //{
    //    std::cout << std::format("Unable to connect: {}\n", inputStream.error().message());
    //    return {};
    //}

    //// Send the request. We specify the "Connection: close" header so that the
    //// server will close the socket after transmitting the response. This will
    //// allow us to treat all data up until the EOF as the content.
    //inputStream << "GET " << kRequest << " HTTP/1.0\r\n";
    //inputStream << "Host: " << kServer << "\r\n";
    //inputStream << "Accept: */*\r\n";
    //inputStream << "Connection: close\r\n\r\n";

    //// By default, the stream is tied with itself. This means that the stream
    //// automatically flush the buffered output before attempting a read. It is
    //// not necessary not explicitly flush the stream at this point.

    //// Check that response is OK.
    //std::string http_version;
    //inputStream >> http_version;
    //unsigned int status_code;
    //inputStream >> status_code;
    //std::string status_message;
    //std::getline(inputStream, status_message);
    //if (!inputStream || http_version.substr(0, 5) != "HTTP/")
    //{
    //    std::cout << "Invalid response\n";
    //    return {};
    //}
    //if (status_code != 200)
    //{
    //    std::cout << "Response returned with status code " << status_code << "\n";
    //    return {};
    //}

    //// Process the response headers, which are terminated by a blank line.
    //std::string header;
    //while (std::getline(inputStream, header) && header != "\r")
    //    std::cout << header << "\n";
    //std::cout << "\n";

    ////Read samples into vector
    //std::istream_iterator<int> inputIter(inputStream);
    //std::vector<int> result;
    //std::copy(inputIter, std::istream_iterator<int>{}, std::back_inserter(result));
    //return result;
    return {};
}

std::vector<int> readFromTestData()
{
    std::istringstream inputStream(
        R"(199
           200
           208
           210
           200
           207
           240
           269
           260
           263)");
    std::istream_iterator<int> inputIter(inputStream);
    std::vector<int> result;
    std::copy(inputIter, std::istream_iterator<int>{}, std::back_inserter(result));
    return result;
}

std::vector<int> readFromInputFile()
{
    std::ifstream inputStream("input.txt");
    std::istream_iterator<int> inputIter(inputStream);
    std::vector<int> result;
    std::copy(inputIter, std::istream_iterator<int>{}, std::back_inserter(result));
    return result;
}
