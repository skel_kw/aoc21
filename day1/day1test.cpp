#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#include "day1impl.h"

TEST_CASE("Day1 Test")
{
    REQUIRE(processData<1, false>(readFromTestData()) == 7);
    REQUIRE(processData<3, false>(readFromTestData()) == 5);
}
