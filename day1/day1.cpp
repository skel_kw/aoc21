#include "day1impl.h"

#include <format>
#include <iostream>

//enum class SourceData
//{
//    TestData,
//    HttpStream,
//    TextFile
//};
//constexpr SourceData k_SourceData = SourceData::TextFile;

constexpr bool VerboseOutput = false;

int main([[maybe_unused]] int argc, [[maybe_unused]] char* argv[])
{
    //int incrementCount = 0;
    //if constexpr (k_SourceData == SourceData::TestData)
    //{
    //    incrementCount = processData<1, VerboseOutput>(readFromTestData());
    //}
    //else if constexpr (k_SourceData == SourceData::HttpStream)
    //{
    //    incrementCount = processData<1, VerboseOutput>(readFromHttpSocket());
    //}
    //else if constexpr (k_SourceData == SourceData::TextFile)
    //{
    //    incrementCount = processData<1, VerboseOutput>(readFromInputFile());
    //}

    std::cout << std::format("Single sample:\nThere are {} measurements larger than the previous measurement.\n", processData<1, VerboseOutput>(readFromInputFile()));

    std::cout << std::format("3 sample sliding window:\nThere are {} measurements larger than the previous measurement.\n", processData<3, VerboseOutput>(readFromInputFile()));

    return 0;
}
