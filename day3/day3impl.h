#pragma once

#include <array>
#include <istream>
#include <vector>

struct DiagnosticReport
{
	int gammaRate = {};
	int epsilonRate = {};
	int GetPowerConsumption() const { return gammaRate * epsilonRate; }

	int oxygenGeneratorRating = {};
	int co2ScrubberRating = {};
	int GetLifeSupportRating() const { return oxygenGeneratorRating * co2ScrubberRating; }

	explicit DiagnosticReport(std::istream&& inputStream);
private:
	static constexpr std::uint32_t BitCount = 12;
	using BitCountArray = std::array<std::uint32_t, BitCount>;

	BitCountArray m_bitCounts = {};
	std::vector<std::uint32_t> m_readings;
	std::uint32_t m_maxBitsUsed = {};
	std::uint32_t m_mostCommonBits = {};

	enum class TreatEqual
	{
		As0,
		As1
	};

	static std::vector<std::uint32_t> ParseReadingStream(std::istream&& inputStream);
	static std::uint32_t CalculateMaxUsedBits(std::vector<std::uint32_t> const& readings);
	static std::array<std::uint32_t, BitCount> CalculateBitCounts(std::vector<std::uint32_t> const& readings, std::uint32_t maxUsedBits);
	static std::uint32_t CalculateMostCommonBits(std::vector<std::uint32_t> const& readings, std::uint32_t maxUsedBits, BitCountArray const& bitCounts, TreatEqual treatEqual);
	void CalculatePowerConsumption();
	void CalculateLifeSupportRating();
};
