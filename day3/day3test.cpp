#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#include "day3impl.h"

const char* k_testData = R"(
00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010
)";

TEST_CASE("Day3 Test")
{
    auto const diags = DiagnosticReport{ std::stringstream(k_testData) };
    REQUIRE(diags.gammaRate == 0b10110);
    REQUIRE(diags.epsilonRate == 0b01001);
    REQUIRE(diags.GetPowerConsumption() == 198);
    REQUIRE(diags.oxygenGeneratorRating == 23);
    REQUIRE(diags.co2ScrubberRating == 10);
    REQUIRE(diags.GetLifeSupportRating() == 230);
}
