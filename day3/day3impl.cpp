#include "day3impl.h"

#include <algorithm>
#include <concepts>
#include <exception>
#include <format>
#include <numeric>

template <std::integral T>
T integralPower(T in, std::make_unsigned_t<T> pow)
{
	if (pow == 0)
	{
		return 1;
	}
	else if (pow > 1)
	{
		return in * integralPower(in, pow - 1);
	}
	else
	{
		return in;
	}
}

DiagnosticReport::DiagnosticReport(std::istream&& inputStream)
{
	m_readings = ParseReadingStream(std::move(inputStream));
	m_maxBitsUsed = CalculateMaxUsedBits(m_readings);
	m_bitCounts = CalculateBitCounts(m_readings, m_maxBitsUsed);
	m_mostCommonBits = CalculateMostCommonBits(m_readings, m_maxBitsUsed, m_bitCounts, TreatEqual::As0);
	CalculatePowerConsumption();
	CalculateLifeSupportRating();
}

std::vector<std::uint32_t> DiagnosticReport::ParseReadingStream(std::istream&& inputStream)
{
	auto transformFunc = [](std::uint64_t val) -> std::uint32_t {
		std::uint32_t res = {};

		//C++ istream has no support for binary formatted input (altho it does support decimal, hex and octal...)
		// So convert the input value into binary
		constexpr int MaxDigits = 12;
		for (int digit = 0; digit < MaxDigits; digit++)
		{
			constexpr std::uint64_t kBase = 10;
			std::uint64_t const maskedVal = (val / integralPower(kBase, digit)) % kBase;
			if (maskedVal == 0 || maskedVal == 1)
			{
				res |= maskedVal << digit;
			}
			else
			{
				throw std::exception(std::format("Bad binary format. Must only contain digits 0 and 1. Value: {:x}", val).c_str());
			}
		}
		return res;
	};

	std::istream_iterator<std::uint64_t> inputIter(inputStream);
	std::vector<std::uint32_t> readings;
	std::transform(inputIter, std::istream_iterator<std::uint64_t>{}, std::back_inserter(readings), transformFunc);
	return readings;
}

std::uint32_t DiagnosticReport::CalculateMaxUsedBits(std::vector<std::uint32_t> const& readings)
{
	std::uint32_t const maxReading = std::reduce(std::begin(readings), std::end(readings), 0u,
		[](std::uint32_t a, std::uint32_t b) { return std::max(a, b); });
	return std::bit_width(maxReading);
}

std::array<std::uint32_t, DiagnosticReport::BitCount> DiagnosticReport::CalculateBitCounts(std::vector<std::uint32_t> const& readings, std::uint32_t maxUsedBits)
{
	if (maxUsedBits > BitCount)
	{
		throw std::exception("Number is too large to represent internally");
	}

	std::array<std::uint32_t, BitCount> bitCounts = {};
	for (std::uint32_t r : readings)
	{
		for (std::uint32_t i = 0; i < maxUsedBits; i++)
		{
			if (r & (1u << i))
			{
				bitCounts[i]++;
			}
		}
	}

	return bitCounts;
}

std::uint32_t DiagnosticReport::CalculateMostCommonBits(std::vector<std::uint32_t> const& readings, std::uint32_t const maxUsedBits, BitCountArray const& bitCounts, TreatEqual treatEqual)
{
	std::uint32_t mcb = {};
    auto const readingCount = readings.size();
	auto const threshold = readingCount / 2;
	for (std::uint32_t i = 0; i < maxUsedBits; i++)
	{
		auto const bitCount = bitCounts[i];
		if (bitCount == (readingCount - bitCount)) //if there the same number of occurrences of 0 and 1
		{
			mcb |= (treatEqual == TreatEqual::As1) ? (1 << i) : 0;
		}
		else
		{
			mcb |= (bitCount > threshold) ? (1 << i) : 0;
		}
	}
	return mcb;
}

void DiagnosticReport::CalculatePowerConsumption()
{
	gammaRate = m_mostCommonBits;
	epsilonRate = (~m_mostCommonBits) & ((1 << m_maxBitsUsed) - 1);
}

void DiagnosticReport::CalculateLifeSupportRating()
{
	//Oxy
	{
		auto filteredReadings = m_readings;
		for (int currentBit = m_maxBitsUsed - 1; (currentBit >= 0) && (filteredReadings.size() > 1); currentBit--)
		{
			BitCountArray const bitCounts = CalculateBitCounts(filteredReadings, m_maxBitsUsed);
			std::uint32_t const mostCommonBits = CalculateMostCommonBits(filteredReadings, m_maxBitsUsed, bitCounts, TreatEqual::As1);

			std::uint32_t const bitMask = 1 << currentBit;
			std::erase_if(filteredReadings, [bitMask, mcb = mostCommonBits](std::uint32_t v) {
				return (v & bitMask) != (mcb & bitMask);
				});
		}

		if (filteredReadings.size() != 1)
		{
			throw std::exception("Error [Oxy]: Too many filtered results remain after filtering");
		}
		else if (filteredReadings.empty())
		{
			throw std::exception("Error [Oxy]: No filtered results remain after filtering");
		}

		oxygenGeneratorRating = filteredReadings[0];
	}

	//CO2
	{
		auto filteredReadings = m_readings;
		for (int currentBit = m_maxBitsUsed - 1; (currentBit >= 0) && (filteredReadings.size() > 1); currentBit--)
		{
			BitCountArray const bitCounts = CalculateBitCounts(filteredReadings, m_maxBitsUsed);
			std::uint32_t const mostCommonBits = CalculateMostCommonBits(filteredReadings, m_maxBitsUsed, bitCounts, TreatEqual::As1);
			std::uint32_t const leastCommonBits = (~mostCommonBits) & ((1 << m_maxBitsUsed) - 1);

			std::uint32_t const bitMask = 1 << currentBit;
			std::erase_if(filteredReadings, [bitMask, lcb = leastCommonBits](std::uint32_t v) {
				return (v & bitMask) != (lcb & bitMask);
				});
		}

		if (filteredReadings.size() != 1)
		{
			throw std::exception("Error [CO2]: Too many filtered results remain after filtering");
		}
		else if (filteredReadings.empty())
		{
			throw std::exception("Error [CO2]: No filtered results remain after filtering");
		}

		co2ScrubberRating = filteredReadings[0];
	}
}
