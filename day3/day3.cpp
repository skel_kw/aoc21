#include "day3impl.h"

#include <format>
#include <fstream>
#include <iostream>

int main([[maybe_unused]] int argc, [[maybe_unused]] char* argv[])
{
    {
        std::ifstream inputStream("input.txt");
        auto const diags = DiagnosticReport{ std::move(inputStream) };
        std::cout << std::format("Part 3a:\nGamma: {:b}\nEpsilon: {:b}\nPower consumption: {}\n\n", diags.gammaRate, diags.epsilonRate, diags.GetPowerConsumption());
        std::cout << std::format("Part 3b:\nOxy: {}\nCO2: {}\nLife support rating: {}\n\n", diags.oxygenGeneratorRating, diags.co2ScrubberRating, diags.GetLifeSupportRating());
    }

    return 0;
}
