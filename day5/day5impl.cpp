#include "day5impl.h"

#include <algorithm>
#include <numeric>
#include <sstream>

bool LineSegment::OverlapsPoint(Point const& p) const
{
    if (IsZeroLength() || !(IsVertical() || IsHorizontal()))
    {
        throw std::exception("Operation not supported on zero length, non-vertical or non-horizontal lines");
    }

    if (IsHorizontal())
    {
        return (p.y == a.y) && (
            ((p.x >= a.x) && (p.x <= b.x)) || ((p.x >= b.x) && (p.x <= a.x)));
    }
    else if (IsVertical())
    {
        return (p.x == a.x) && (
            ((p.y >= a.y) && (p.y <= b.y)) || ((p.y >= b.y) && (p.y <= a.y)));
    }
    else
    {
        return false;
    }
}

OverlapReport::OverlapReport(std::vector<LineSegment>&& lines_, int gridDimX_, int gridDimY_)
    : lines(std::move(lines_))
    , gridDimX(gridDimX_)
    , gridDimY(gridDimY_)
{

}

std::vector<OverlapReport::OverlapPointCount> OverlapReport::GetOverlapCounts() const
{
    std::vector<OverlapReport::OverlapPointCount> res;

    for (int y = 0; y < gridDimY; y++)
    {
        for (int x = 0; x < gridDimX; x++)
        {
            auto const p = Point(x, y);
            auto const overlapCount = std::count_if(std::begin(lines), std::end(lines), [p](LineSegment const& ls) { return ls.OverlapsPoint(p); });
            res.emplace_back(p, static_cast<int>(overlapCount));
        }
    }

    return res;
}

std::string OverlapReport::GetOverlapReport() const
{
    std::stringstream res;
    for (int y = 0; y < gridDimY; y++)
    {
        for (int x = 0; x < gridDimX; x++)
        {
            auto const p = Point(x, y);
            auto const overlapCount = std::count_if(std::begin(lines), std::end(lines), [p](LineSegment const& ls) { return ls.OverlapsPoint(p); });
            if (overlapCount > 0)
            {
                res << overlapCount;
            }
            else
            {
                res << '.';
            }
        }
        res << '\n';
    }
    return res.str();
}

std::vector<LineSegment> ParseLineSegments(std::istream& inputStream)
{
    //Read in line segments, of the format: "X0,Y0 -> X1,Y1" separated by lines
    if (inputStream.bad())
    {
        throw std::exception("Input buffer not valid");
    }

    //read any leading whitespace
    inputStream >> std::ws;

    std::vector<LineSegment> res;
    while (inputStream.good())
    {
        std::string readBuffer;
        std::getline(inputStream >> std::ws, readBuffer);
        if (inputStream.bad())
        {
            throw std::exception("Input buffer error when reading numbers");
        }

        std::replace(std::begin(readBuffer), std::end(readBuffer), ',', ' ');
        std::replace(std::begin(readBuffer), std::end(readBuffer), '-', ' ');
        std::replace(std::begin(readBuffer), std::end(readBuffer), '>', ' ');

        std::stringstream strStream(readBuffer);
        int x0{}, y0{}, x1{}, y1{};
        strStream >> x0 >> y0 >> x1 >> y1;
        if (inputStream.bad())
        {
            throw std::exception("Bad line segment format in input stream");
        }
        res.emplace_back(Point{ x0, y0 }, Point{ x1, y1 });
    }
    return res;
}
