#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#include "day5impl.h"

#include <sstream>
#include <string_view>

const char* k_testData = R"(
0,9 -> 5,9
8,0 -> 0,8
9,4 -> 3,4
2,2 -> 2,1
7,0 -> 7,4
6,4 -> 2,0
0,9 -> 2,9
3,4 -> 1,4
0,0 -> 8,8
5,5 -> 8,2
)";

const char* k_expectedLineCoverage = R"(
.......1..
..1....1..
..1....1..
.......1..
.112111211
..........
..........
..........
..........
222111....
)";

TEST_CASE("Day5a Test")
{
    auto inputData = std::stringstream(k_testData);

    std::vector<LineSegment> lineSegments = ParseLineSegments(inputData);

    std::erase_if(lineSegments, [](LineSegment const& ls) { return ls.IsZeroLength() || !(ls.IsHorizontal() || ls.IsVertical()); });

    //filter out lines that aren't horizontal or vertical
    constexpr int dimX = 10;
    constexpr int dimY = 10;
    auto const overlapReport = OverlapReport(std::move(lineSegments), dimX, dimY);

    auto const overlapCounts = overlapReport.GetOverlapCounts();

    auto const numberOfMultipleOverlaps =
        std::count_if(std::begin(overlapCounts), std::end(overlapCounts), [](auto const& olc) { return olc.second >= 2; });
    REQUIRE(numberOfMultipleOverlaps == 5);

    std::string const report = overlapReport.GetOverlapReport();
    auto trimWhitespace = [](std::string_view s) -> std::string_view {
        while (std::isspace(s.front()))
        {
            s.remove_prefix(1);
        }
        while (std::isspace(s.back()))
        {
            s.remove_suffix(1);
        }
        return s;
    };

    REQUIRE(trimWhitespace(report).compare(trimWhitespace(k_expectedLineCoverage)) == 0);
}

//TEST_CASE("Day5b Test")
//{
//    auto inputData = std::stringstream(k_testData);
//
//}
