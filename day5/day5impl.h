#pragma once

#include <string>
#include <utility>
#include <vector>

class Point
{
public:
    int x = {};
    int y = {};

    Point(int x_, int y_) : x(x_), y(y_) {}

    [[nodiscard]] friend bool operator==(Point const& a, Point const& b) { return a.x == b.x && a.y == b.y; }
};

class LineSegment
{
    Point a;
    Point b;

public:
    LineSegment(Point a_, Point b_) : a(a_), b(b_) {}

    [[nodiscard]] bool IsZeroLength() const { return a == b; }
    [[nodiscard]] bool IsVertical() const { return a.x == b.x; }
    [[nodiscard]] bool IsHorizontal() const { return a.y == b.y; }

    [[nodiscard]] bool OverlapsPoint(Point const& p) const;
};

class OverlapReport
{
    using OverlapPointCount = std::pair<Point, int>;

    const std::vector<LineSegment> lines;
    const int gridDimX = {};
    const int gridDimY = {};

public:
    explicit OverlapReport(std::vector<LineSegment>&& lines_, int gridDimX_, int gridDimY_);

    [[nodiscard]] std::vector<OverlapPointCount> GetOverlapCounts() const;
    [[nodiscard]] std::string GetOverlapReport() const;
};

[[nodiscard]] std::vector<LineSegment> ParseLineSegments(std::istream& inputStream);
